package encapsulation;

import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class Person {
	private String name;
	private String email;
	private Date birthday;
	private char gender;
	
	private static final Set<String> VALID_CODES = new HashSet<>(Arrays.asList("ad", "ae", "af", "ag", "ai", "al", "am", "ao", "aq", "ar", "as", "at", "au", "aw", "ax", "az", "ba", "bb", "bd", "be", "bf", "bg", "bh", "bi", "bj", "bl", "bm", "bn", "bo", "bq", "br", "bs", "bt", "bv", "bw", "by", "bz", "ca", "cc", "cd", "cf", "cg", "ch", "ci", "ck", "cl", "cm", "cn", "co", "cr", "cu", "cv", "cw", "cx", "cy", "cz", "de", "dj", "dk", "dm", "do", "dz", "ec", "ee", "eg", "eh", "er", "es", "et", "fi", "fj", "fk", "fm", "fo", "fr", "ga", "gb", "gd", "ge", "gf", "gg", "gh", "gi", "gl", "gm", "gn", "gp", "gq", "gr", "gs", "gt", "gu", "gw", "gy", "hk", "hm", "hn", "hr", "ht", "hu", "id", "ie", "il", "im", "in", "io", "iq", "ir", "is", "it", "je", "jm", "jo", "jp", "ke", "kg", "kh", "ki", "km", "kn", "kp", "kr", "kw", "ky", "kz", "la", "lb", "lc", "li", "lk", "lr", "ls", "lt", "lu", "lv", "ly", "ma", "mc", "md", "me", "mf", "mg", "mh", "mk", "ml", "mm", "mn", "mo", "mp", "mq", "mr", "ms", "mt", "mu", "mv", "mw", "mx", "my", "mz", "na", "nc", "ne", "nf", "ng", "ni", "nl", "no", "np", "nr", "nu", "nz", "om", "pa", "pe", "pf", "pg", "ph", "pk", "pl", "pm", "pn", "pr", "ps", "pt", "pw", "py", "qa", "re", "ro", "rs", "ru", "rw", "sa", "sb", "sc", "sd", "se", "sg", "sh", "si", "sj", "sk", "sl", "sm", "sn", "so", "sr", "ss", "st", "sv", "sx", "sy", "sz", "tc", "td", "tf", "tg", "th", "tj", "tk", "tl", "tm", "tn", "to", "tr", "tt", "tv", "tw", "tz", "ua", "ug", "um", "us", "uy", "uz", "va", "vc", "ve", "vg", "vi", "vn", "vu", "wf", "ws", "ye", "yt", "za", "zm", "zw"));
	private static final Set<Character> VALID_GENDERS = new HashSet<>(Arrays.asList('M', 'F', '\0'));
	
	
	private void checkName(String name) throws IllegalArgumentException {
		String[] names = name.split(" ");
		
		// Make sure there is a first and last name.
		if (names.length != 2)
			throw new IllegalArgumentException("Name must consist of both first and last name.");
		
		String fName = names[0];
		String lName = names[1];
		
		// First and last name must consist of at least two characters
		if (fName.length() < 2  ||  lName.length() < 2)
			throw new IllegalArgumentException("First and last name must consist of at least two characters.");
		
		// Names must consist of letters only
		if (!fName.chars().allMatch(Character::isLetter))
			throw new IllegalArgumentException("First name must consist of characters only.");
		
		if (!lName.chars().allMatch(Character::isLetter))
			throw new IllegalArgumentException("Last name must consist of characters only.");
	}		
	
	private void checkEmail(String email) throws IllegalArgumentException {
		String[] splitEmail = email.split("@");
		
		// Make sure email contains '@'.
		if (splitEmail.length != 2)
			throw new IllegalArgumentException("Email must contain a '@' character.");
		
		String prefix = splitEmail[0];
		String suffix = splitEmail[1];
		String[] names = prefix.split("\\.");
		
		// Make sure prefix is divided into first and last name.
		if (names.length != 2)
			throw new IllegalArgumentException("Email prefix must consist of first and last name.");
		
		String nameFromPrefix = String.join(" ", names);
		
		// Prefix must match registered name.
		if (!nameFromPrefix.equalsIgnoreCase(name))
			throw new IllegalArgumentException("Email prefix does not match registered name.");
		
		String[] splitSuffix = suffix.split("\\.");
		
		// Make sure suffix is divided into domain and landcode.
		if (splitSuffix.length != 2)
			throw new IllegalArgumentException("Email suffix must consist of domain and landcode.");
		
		String domain = splitSuffix[0];
		String landcode = splitSuffix[1];
		
		// Make sure domain consists of letters and digits only.
		if (!domain.chars().allMatch(Character::isLetterOrDigit))
			throw new IllegalArgumentException("Domain must consist of letters and digits only.");
		
		// Make sure landcode is valid
		if (!VALID_CODES.contains(landcode))
			throw new IllegalArgumentException("Landcode is invalid.");
	}
	
	private void checkBirthday(Date birthday) throws IllegalArgumentException {
		Date now = new Date();
		
		// Make sure that birthday is in the past
		if (!now.after(birthday))
			throw new IllegalArgumentException("Birthday cannot be forward in time.");
	}
	
	private void checkGender(char gender) throws IllegalArgumentException {
		// Make sure gender is present in set of valid genders
		if (!VALID_GENDERS.contains(gender))
			throw new IllegalArgumentException("Gender is invalid.");
	}
	
	public String getName() {
		return name;
	}
	
	public String getEmail() {
		return email;
	}
	
	public Date getBirthday() {
		return birthday;
	}
	
	public char getGender() {
		return gender;
	}
	
	public void setName(String name) {
		checkName(name);
		this.name = name;
	}
	
	public void setEmail(String email) {
		checkEmail(email);
		this.email = email;
	}
	
	public void setBirthday(Date birthday) {
		checkBirthday(birthday);
		this.birthday = birthday;
	}
	
	public void setGender(char gender) {
		checkGender(gender);
		this.gender = gender;
	}
	
}
