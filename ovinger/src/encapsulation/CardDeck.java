package encapsulation;

import java.util.ArrayList;
import java.util.List;

public class CardDeck {
	private List<Card> cards = new ArrayList<>();
	
	
	public CardDeck(int n) {
		for (char suit : Card.validSuits)
			for (int face = 1; face <= n; face++)
				cards.add(new Card(suit, face));
	}
	
	
	public int getCardCount() {
		return cards.size();
	}
	
	public Card getCard(int n) {
		if (n < 0  ||  n >= cards.size())
			throw new IllegalArgumentException("Invalid card index.");
		
		return cards.get(n);
	}
	
	public void shufflePerfectly() {
		int splitIndex = cards.size() / 2;
		ArrayList<Card> shuffled = new ArrayList<>();
		List<Card> cards1 = cards.subList(0, splitIndex);
		List<Card> cards2 = cards.subList(splitIndex, cards.size());
		
		for (int i = 0; i < splitIndex; i++) {
			shuffled.add(cards1.get(i));
			shuffled.add(cards2.get(i));
		}
		
		this.cards = shuffled;
	}
}
