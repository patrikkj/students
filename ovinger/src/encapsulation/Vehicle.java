package encapsulation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Vehicle {
	private final char vehicleType;
	private final char fuelType;
	private String registrationNumber;
	
	private static final Set<Character> validVehicleTypes = new HashSet<>(Arrays.asList('M', 'C'));
	private static final Set<Character> validFuelTypes = new HashSet<>(Arrays.asList('H', 'E', 'D', 'G'));
	private static final List<String> illegalSubstrings = new ArrayList<>(Arrays.asList("EL", "EK", "HY", "Æ", "Ø", "Å"));
	
	
	public Vehicle(char vehicleType, char fuelType, String registrationNumber) {
		checkVehicle(vehicleType, fuelType, registrationNumber);
		
		this.vehicleType = vehicleType;
		this.fuelType = fuelType;
		this.registrationNumber = registrationNumber;
	}

	
	private boolean isValidVehicleType(char vehicleType) {
		return validVehicleTypes.contains(vehicleType);
	}
	
	private boolean isValidFuelType(char fuelType) {
		return validFuelTypes.contains(fuelType);
	}
	
	private boolean isValidRegistrationNumber(char vehicleType, char fuelType, String registrationNumber) {
		// Verify length to prevent IndexOutOfBoudsException.
		if (registrationNumber.length() != 6  &&  registrationNumber.length() != 7)
			return false;
		
		String regPrefix = registrationNumber.substring(0, 2);
		String regSuffix = registrationNumber.substring(2);
		
		// Electric vehicles' regNums are required to start with "EL" or "EK".
		if (fuelType == 'E'  &&  (!regPrefix.equals("EL")  &&  !regPrefix.equals("EK")))
			return false;
		
		// Hydrogen vehicles' regNums are required to start with "HY".
		if (fuelType == 'H'  && !regPrefix.equals("HY"))
			return false;
		
		// Diesel and gas vehicles' regNums cannot contain "EL", "EK", "HY", "Æ", "Ø" or "Å".
		if ((fuelType == 'D'  ||  fuelType == 'G')  &&  illegalSubstrings.stream().anyMatch(s -> regPrefix.contains(s)))
			return false;
			
		// Registration number must start with two letters.
		if (!regPrefix.chars().allMatch(Character::isLetter))
			return false;
		
		// Registration number prefix must be in upper case.
		if (!regPrefix.chars().allMatch(Character::isUpperCase))
			return false;
		
		// Registration number suffix must be digits only.
		if (!regSuffix.chars().allMatch(Character::isDigit))
			return false;
		
		// Motorcycles has a suffix length of four digits.
		if (vehicleType == 'M'  &&  regSuffix.length() != 4)
			return false;
		
		// Cars has a suffix length of five characters.
		if (vehicleType == 'C'  &&  regSuffix.length() != 5)
			return false;
		
		// Motorcycles cannot be fueled by hydrogen.
		if (vehicleType == 'M'  &&  fuelType == 'H')
			return false;
		
		return true;
	}
	
	private void checkVehicle(char vehicleType, char fuelType, String registrationNumber) {
		if (!isValidVehicleType(vehicleType))
			throw new IllegalArgumentException("Invalid vehicle type.");
		
		if (!isValidFuelType(fuelType))
			throw new IllegalArgumentException("Invalid fuel type.");
		
		if (!isValidRegistrationNumber(vehicleType, fuelType, registrationNumber))
			throw new IllegalArgumentException("Invalid registration number.");
	}
	
	public void setRegistrationNumber(String registrationNumber) {
		if (!isValidRegistrationNumber(vehicleType, fuelType, registrationNumber))
			throw new IllegalArgumentException("Invalid registration number.");
		
		this.registrationNumber = registrationNumber;
	}
	
	public char getVehicleType() {
		return vehicleType;
	}

	public char getFuelType() {
		return fuelType;
	}
	
	public String getRegistrationNumber() {
		return registrationNumber;
	}
}
