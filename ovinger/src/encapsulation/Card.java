package encapsulation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Card {
	private final char suit;
	private final int face;
	
	protected static final List<Character> validSuits = new ArrayList<>(Arrays.asList('S', 'H', 'D', 'C'));
	
	
	public Card(char suit, int face) {
		if (!isValidSuit(suit))
			throw new IllegalArgumentException(String.format("Invalid suit: %s", suit));
		
		if (!isValidFace(face))
			throw new IllegalArgumentException(String.format("Invalid face: %s", face));
		
		this.suit = suit;
		this.face = face;
	}
	
	private boolean isValidSuit(char suit) {
		return validSuits.contains(suit);
	}
	
	private boolean isValidFace(int face) {
		return face >= 1  &&  face <= 13; 
	}
	
	public char getSuit() {
		return suit;
	}
	
	public int getFace() {
		return face;
	}

	@Override
	public String toString() {
		return String.format("%s%s", suit, face);
	}
}
