package encapsulation;

import java.util.Arrays;

public class Nim {
	private final int[] piles = new int[3];
	private final int pileSize;
	
	
	public Nim() {
		this(10);
	}
	
	public Nim(int pileSize) {
		this.pileSize = pileSize;
		Arrays.fill(piles, pileSize);
	}
	
	
	public void removePieces(int number, int targetPile) {
		if (isGameOver())
			throw new IllegalStateException("Cannot remove pieces after game has finished.");
		
		if (!isValidMove(number, targetPile))
			throw new IllegalArgumentException("Invalid move.");
		
		piles[targetPile] -= number;
	}
	
	public boolean isValidMove(int number, int targetPile) {
		if (isGameOver())
			return false;
		
		if (number < 1)
			return false;
		
		if (targetPile < 0  ||  targetPile > 2)
			return false;
		
		if (piles[targetPile] < number)
			return false;
		
		return true;
	}
	
	public boolean isGameOver() {
		return Arrays.stream(piles).anyMatch(i -> i == 0);
	}
	
	public int getPile(int targetPile) {
		return piles[targetPile];
	}


	@Override
	public String toString() {
		return "Nim [piles=" + Arrays.toString(piles) + ", pileSize=" + pileSize + "]";
	}
	
	
	
}
