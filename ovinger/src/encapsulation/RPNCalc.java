package encapsulation;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;
import java.util.function.DoubleBinaryOperator;
import java.util.function.DoubleSupplier;
import java.util.function.DoubleUnaryOperator;

public class RPNCalc {
	private Stack<Double> stack = new Stack<>();

	// Operator mappings
	private static Map<Character, DoubleBinaryOperator> binaryOperators = new HashMap<>();
	private static Map<Character, DoubleUnaryOperator> unaryOperators = new HashMap<>();
	private static Map<Character, DoubleSupplier> suppliers = new HashMap<>();

	// Static initialization block
	static {
		// Binary operators
		binaryOperators.put('+', (a, b) -> a + b);
		binaryOperators.put('-', (a, b) -> a - b);
		binaryOperators.put('*', (a, b) -> a * b);
		binaryOperators.put('/', (a, b) -> a / b);
		binaryOperators.put('%', (a, b) -> a % b);
		binaryOperators.put('^', Math::pow);

		// Unary operators
		unaryOperators.put('|', a -> (a < 0) ? -a : a);

		// Suppliers
		suppliers.put('π', () -> Math.PI);
		suppliers.put('e', () -> Math.E);
	}

	
	public void push(double item) {
		stack.push(item);
	}

	public double pop() {
		return stack.isEmpty() ? Double.NaN : stack.pop();
	}

	public double peek(int depth) {
		if (depth >= getSize()  ||  depth < 0) 
			return Double.NaN;
		
		return stack.get(getSize() - depth - 1);
	}

	public int getSize() {
		return stack.size();
	}

	public void performOperation(char operator) {
		// Binary operators
		switch (operator) {
		case '+':
			performBinaryOperation('+', 0);
			break;
		case '-':
			performBinaryOperation('-', 0);
			break;
		case '*':
			performBinaryOperation('*', 1);
			break;
		case '/':
			performBinaryOperation('/', 1);
			break;
		case '%':
			performBinaryOperation('%', 1);
			break;
		case '^':
			performBinaryOperation('^', 1);
			break;
		
		// Unary operators
		case '|':
			performUnaryOperation('|');
			break;
			
		// Suppliers
		case 'π':
			performSupplier('π');
			break;
		case 'e':
			performSupplier('e');
			break;
			
		// Special
		case '~':
			performSpecial('~');
			break;
			
		default:
			throw new IllegalArgumentException(String.format("Operation '%s' not supported.", operator));
		}
		
		
	}
	
	private void performBinaryOperation(char operator, double defaultParam) {
		double a = pop();
		double b = pop();

		if (a == Double.NaN)
			throw new IllegalStateException("Binary operations cannot be missing both operands.");
		
		if (b == Double.NaN)
			b = defaultParam;
		
		push(binaryOperators.get(operator).applyAsDouble(b, a));
	}
	
	private void performUnaryOperation(char operator) {
		double a = pop();
		
		if (a == Double.NaN)
			throw new IllegalStateException("Unary operations requires at least one operand on the stack.");
		
		push(unaryOperators.get(operator).applyAsDouble(a));
		
	}
	
	private void performSupplier(char operator) {
		push(suppliers.get(operator).getAsDouble());
	}
	
	private void performSpecial(char operator) {
		switch (operator) {
		case '~':
			if (getSize() < 2)
				throw new IllegalArgumentException("Swap is not possible with less than two operands.");
			
			double op1 = pop();
			double op2 = pop();
			
			push(op1);
			push(op2);
			break;

		default:
			break;
		}
	}
}
