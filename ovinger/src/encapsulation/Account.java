package encapsulation;

public class Account {
	private double balance;
	private double interestRate;
	
	
	public Account(double balance, double interestRate) {
		if (balance < 0)
			throw new IllegalArgumentException("Balance must be non-negative.");
		if (interestRate < 0)
			throw new IllegalArgumentException("Interest rate must be positive.");
		
		this.balance = balance;
		this.interestRate = interestRate;
	}

	
	public double getBalance() {
		return balance;
	}
	
	public double getInterestRate() {
		return interestRate;
	}
	
	public void setInterestRate(double interestRate) {
		if (interestRate < 0)
			throw new IllegalArgumentException("Interest rate must be positive.");
		
		this.interestRate = interestRate;
	}
	
	public void deposit(double amount) {
		if (amount < 0)
			throw new IllegalArgumentException("Deposit amount must be positive.");
		this.balance += amount;
	}
	
	public void withdraw(double amount) {
		if (amount < 0)
			throw new IllegalArgumentException("Withdrawal amount must be positive.");
		else if (balance < amount)
			throw new IllegalArgumentException("Your balance is too low for this withdrawal.");
		
		this.balance -= amount;
	}
	
	public void addInterest() {
		this.balance *= (1 + interestRate/100);
	}
}
